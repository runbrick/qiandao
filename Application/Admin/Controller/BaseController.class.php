<?php

namespace Admin\Controller;

use Think\Controller;

abstract class BaseController extends Controller
{
    /**
     * [_initialize 实例化公共方法]
     * @author runbrick <zhouwei9280@gmail.com>
     * @version 1.0.0
     */
    public function _initialize()
    {
        $this->isLogin();
        $this->assign('admin_account', json_decode(session('AdminID'), true));
        $this->assign('thisUrl',CONTROLLER_NAME.'/'.ACTION_NAME);
    }

    /**
     * [add 新增]
     * @author runbrick <zhouwei9280@gmail.com>
     * @version 1.0.0
     */
    abstract public function add();

    /**
     * [edit 修改]
     * @author runbrick <zhouwei9280@gmail.com>
     * @version 1.0.0
     */
    abstract public function edit();

    /**
     * [index 列表]
     * @author runbrick <zhouwei9280@gmail.com>
     * @version 1.0.0
     */
    abstract public function index();

    /**
     * [post 新增/修改 ]
     * @author runbrick <zhouwei9280@gmail.com>
     * @version 1.0.0
     */
    abstract public function post();

    /**
     * [isLogin 判断是否登录]
     * @author runbrick <zhouwei9280@gmail.com>
     * @version 1.0.0
     */
    public function isLogin()
    {
        session('[regenerate]');
        $isLogin = json_decode(session('AdminID'), true);
        if ( empty($isLogin['account']) ) {
            $this->error('请先登录', U('Login/index'));
        }
    }
}