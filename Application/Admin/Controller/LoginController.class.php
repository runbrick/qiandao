<?php

namespace Admin\Controller;

use Think\Controller;

class LoginController extends Controller
{
    public function index()
    {
        $this->display();
    }

    /**
     * [login 执行登录]
     * @author runbrick <zhouwei9280@gmail.com>
     * @version 1.0.0
     */
    public function login()
    {
        if ( IS_POST ) {
            $account = I('account');
            $password = I('password');
            if ( empty($account) ) {
                ajaxReturn(1, '请输入账号', array());
            }
            if ( empty($password) ) {
                ajaxReturn(1, '请输入密码', array());
            }
            $param['where'] = array('account' => $account);
            $passwordr = D('AdminAccount')->getInfo($param);
            if ( !$passwordr['password'] ) {
                ajaxReturn(1, '该账号不存在', array());
            } else {
                if ( $passwordr['password'] != encrypt($password) ) {
                    ajaxReturn(1, '请输入正确的密码', array());
                } else {
                    // 更新登录时间
                    D('AdminAccount')->newLastLoginTime($account);
                    session('AdminID', json_encode($passwordr));
                    ajaxReturn(0, '登录成功', array());
                }
            }

        } else {
            ajaxReturn(1, '请求方式有误');
        }
    }

    /**
     * [logout 退出登录]
     * @author runbrick <zhouwei9280@gmail.com>
     * @version 1.0.0
     */
    public function logout()
    {
        $this->success('注销登录成功', U('login/index'));
    }

}