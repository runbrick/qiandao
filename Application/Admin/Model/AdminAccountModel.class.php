<?php

namespace Admin\Model;

use Think\Model;

/**
 * Class AdminAccountModel [管理员账号]
 * @package Admin\Model
 * author runbrick <zhouwei9280@gmail.com>
 */
class AdminAccountModel extends Model
{
    /**
     * [getInfo 获取管理员详情]
     * @author runbrick <zhouwei9280@gmail.com>
     * @version 1.0.0
     * @param $param
     */
    public function getInfo($param)
    {
        $info = $this->where($param['where'])->find();
        if ( !$info ) {
            $info = array();
        }
        return $info;
    }

    /**
     * [newLastLoginTime 更新登录时间]
     * @author runbrick <zhouwei9280@gmail.com>
     * @version 1.0.0
     */
    public function newLastLoginTime($account)
    {
        $this->where(array('account' => $account))->save(array('last_login' => time()));
    }

}