<?php

namespace Admin\Model;

use Think\Model;

/**
 * Class ActivityModel [活动管理]
 * @package Admin\Model
 * author runbrick <zhouwei9280@gmail.com>
 */
class ActivityModel extends Model
{

    protected $_validate = array(
        array('title', 'require', '标题不能为空！'),
        array('num', 'require', '请输入活动人数！'),
        array('content', 'require', '请输入活动主题！'),
        array('activity_time', 'require', '请输入开始时间！'),
    );
    protected $_auto = array(
        array('status', 0, 1),
        array('utime', 'time', 2, 'function'),
        array('ctime', 'time', 1, 'function'),
    );
}