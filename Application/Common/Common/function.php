<?php

/**
 * [ajaxReturn ]
 * @author runbrick <zhouwei9280@gmail.com>
 * @version 1.0.0
 * @param $errcode 错误码
 * @param $msg 错误信息
 * @param $data 返回数据
 * @example
 *      ajaxReturn(0,"成功",["a"=>1]); 成功并返回 a=1
 */
function ajaxReturn($errcode, $msg, $data)
{
    die(json_encode(array('errcode' => $errcode, 'msg' => $msg, 'data' => $data)));
}

/**
 * [encrypt 加密]
 * @author runbrick <zhouwei9280@gmail.com>
 * @version 1.0.0
 * @param $ciphertext 加密密文
 */
function encrypt($ciphertext)
{
    return md5(C('PASS_TOKEN') . $ciphertext . C('PASS_TOKEN'));
}

/**
 * [channelExcel 导入Excel]
 * @author runbrick <zhouwei9280@gmail.com>
 * @version 1.0.0
 */
function channelExcel($filename = '')
{
    if ( !is_file($filename) ) {
        return false;
    }
    vendor('PHPExcel.PHPExcel');  //导入thinkphp 中第三方插件库
    vendor('PHPExcel.IOFactory');
    $reader = \PHPExcel_IOFactory::createReader('Excel2007');
    $excel = $reader->load($filename);
    $objWorksheet = $excel->getActiveSheet();
    $highestRow = $objWorksheet->getHighestRow();
    $highestColumn = $objWorksheet->getHighestColumn();
    $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn);
    $excelData = array();
    for ( $row = 1; $row <= $highestRow; $row++ ) {
        for ( $col = 0; $col < $highestColumnIndex; $col++ ) {
            $excelData[$row][] = (string)$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
        }
    }
    return $excelData;
}

function httpsRequest($url, $data = null)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    if ( !empty($data) ) {
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($curl);
    curl_close($curl);
    return $output;
}

