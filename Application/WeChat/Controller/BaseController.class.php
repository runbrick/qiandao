<?php

namespace WeChat\Controller;

use Think\Controller;

class BaseController extends Controller
{
    private $_appid;
    private $_secret;

    public function _initialize()
    {
        $this->_appid = 'wx8334ccec163f7b55';
        $this->_secret = '356d02786ff252bda610e37e7a4c535a';
        $redirect_uri = urlencode('http://' . $_SERVER['SERVER_NAME'] . $_SERVER["REQUEST_URI"]);

        $member = session('Member');
        if ( !$member['openid'] ) {
            if ( !$_GET['code'] ) {
                $url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" . $this->_appid . "&redirect_uri=$redirect_uri&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect";
                header("Location:" . $url);
            } else {
                // 通过Code 获取Token
                $token = json_decode($this->getToken($_GET['code']), true);
                $openid = $token['openid'];
                $access_token = $token["access_token"];
                $verify = json_decode($this->verify($access_token, $openid), true);
                if ( $verify['errcode'] != 0 ) {
                    // 刷新 Token 重新验证
                    $token = json_decode($this->refreshToken($token['refresh_token']), true);
                    $access_token = $token["access_token"];
                }
                $userinfo = json_decode($this->getUser($access_token, $openid), true);
                if ( !empty($userinfo['openid']) ) {
                    session('Member', $userinfo);
                } else {
                    $this->redirect('Index/index');
                }
            }

        }
    }

    public function getToken($code)
    {
//        $url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='. $this->_appid . '&secret=APPSECRET';
        $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" . $this->_appid . "&secret=" . $this->_secret . "&code=$code&grant_type=authorization_code";
        $oauth2 = httpsRequest($url);
        return $oauth2;
    }

    public function getUser($access_token, $openid)
    {
//        $get_user_info_url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token='.$access_token.'&openid='.$openid.'&lang=zh_CN';
        $get_user_info_url = "https://api.weixin.qq.com/sns/userinfo?access_token=$access_token&openid=$openid&lang=zh_CN";
        $userinfo = httpsRequest($get_user_info_url);
        return $userinfo;
    }

    public function refreshToken($refresh_token)
    {
        $url = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=" . $this->_appid . "&grant_type=refresh_token&refresh_token=$refresh_token";
        $oauth2 = httpsRequest($url);
        return $oauth2;
    }

    public function verify($access_token, $openid)
    {
        $url = "https://api.weixin.qq.com/sns/auth?access_token=$access_token&openid=$openid";
        $oauth2 = httpsRequest($url);
        return $oauth2;
    }

}