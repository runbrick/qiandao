<?php

namespace WeChat\Controller;

class IndexController extends BaseController
{
    private $_model;

    public function _initialize()
    {
//        parent::_initialize();
        $this->_model = D('Member');
    }

    public function index()
    {
        $this->display();
    }

    public function show()
    {
        $this->display();
    }

    public function login()
    {
        $member = session('Member');
        $phone = $_GET['phone'];
        if ( empty($phone) ) {
            $this->error('请输入手机号');
        }
        $name = $_GET['name'];
        if ( empty($name) ) {
            $this->error('请输入姓名');
        }
        $info = $this->_model->where(array('phone' => $phone, 'name' => $name))->getField('id');
        // 执行签到
        if ( $info ) {
            $signIn = $this->_model->where(array('id' => $info))->save(array(
                'status' => 1, 'head_pic' => $member['headimgurl'], 'nickname' => $member['nickname'], 'openid' => $member['openid']
            ));
            if ( $signIn ) {
                $this->success('签到成功');
            }
        }
        $this->success('记录不存在');
    }


}