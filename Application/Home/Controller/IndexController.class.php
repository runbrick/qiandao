<?php

namespace Home\Controller;

use Think\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $this->display();
    }


    /**
     * [getJson 获取用户数据]
     * @author runbrick <zhouwei9280@gmail.com>
     * @version 1.0.0
     */
    public function getJson()
    {
        $num = intval($_GET['num']); // 初始化读取条数
        $member = session('memberList');
        if ( $member ) {
            // 查询当前数量是否更新
            $maxId = end($member)['id'];
            $maxDbId = D('Member')->getField('Max(id)');
            if ( $maxId < $maxDbId ) {
                $member = D('Member')->where(array('status' => 1))->field('name,id,head_pic')->order('id ASC')->select();
                session('memberList', $member);
            }

        } else {
            $member = D('Member')->where(array('status' => 1))->field('name,id,head_pic')->order('id ASC')->select();
            session('memberList', $member);
        }
        $count = count($member) - 1;
        if ( $num < $count ) {
            $num += 1;
        } else {
            $num = 0;
        }
        $info = array(
            'info' => $member[$num]['name'] . '进场，欢迎您的到来！',
            'img' => $member[$num]['head_pic'],
        );
        echo json_encode(array('num' => $num, 'info' => $info));
    }
}